//При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
//При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.

function createElement(
  elementName,
  className = "",
  type = "",
  placeholder = "",
  value = ""
) {
  const element = document.createElement(elementName);
  element.className = className;
  element.placeholder = placeholder;
  element.type = type;
  element.value = value;
  return element;
}

const btn = document.querySelector("#btn"),
  input = createElement("input", "input-btn", "text", "Напиши діаметр круга"),
  button = createElement("input", "btn", "button", "", "Зберігти"),
  container = createElement("div", "container");
let size = "",
  circle = 0;

function createCircle() {
  for (let i = 0; i < 100; i++) {
    circle = createElement("div", "circle");
    circleStyle(circle);
    container.append(circle);
  }
}

function circleStyle(element) {
  let color = `hsl(${Math.floor(Math.random() * 360)}, 80%, 30%)`;
  element.style.width = `${size}px`;
  element.style.height = `${size}px`;
  element.style.backgroundColor = color;
}

window.onload = () => {
  btn.onclick = () => {
    btn.after(input);
    input.after(button);
    button.after(container);
  };

  button.onclick = () => {
    size = input.value;
    createCircle();
  };

  container.onclick = (event) => {
    if (event.target.classList.contains("circle")) event.target.remove();
  };
};
