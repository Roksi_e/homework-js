function createElement(
  elementName,
  className = "",
  content = "",
  src = "",
  id = ""
) {
  const element = document.createElement(elementName);
  element.className = className;
  element.textContent = content;
  element.src = src;
  element.id = id;
  return element;
}

class PizzaGroup {
  constructor(div2, div3, button1, button2, button3, img, pissaPrice, counter) {
    this.div2 = div2;
    this.div3 = div3;
    this.button1 = button1;
    this.button2 = button2;
    this.button3 = button3;
    this.img = img;
    this.pissaPrice = pissaPrice;
    this.counter = 0;
  }
  createDiv() {
    this.div2.append(this.div3);
    this.button1.textContent = `${this.counter}`;
    return this.div3.append(this.button2, this.button1, this.button3);
  }
}

function CustomerCard(name, telNumber, email) {
  this.name = name;
  this.telephon = telNumber;
  this.email = email;
}

const data = {},
  dataBase = [];

let elements = [];
elements.push(
  new PizzaGroup(
    createElement("div", "div2", "Кетчуп"),
    createElement("div", "div3"),
    createElement("button", "btn-result"),
    createElement("button", "btn-result", "-"),
    createElement("button", "btn-result", "+"),
    createElement(
      "img",
      "yellow-elemnt",
      undefined,
      "Pizza_pictures/sous-klassicheskij_1557758736353.png",
      "sauceClassic"
    ),
    "15"
  )
),
  elements.push(
    new PizzaGroup(
      createElement("div", "div2", "Соус BBQ"),
      createElement("div", "div3"),
      createElement("button", "btn-result"),
      createElement("button", "btn-result", "-"),
      createElement("button", "btn-result", "+"),
      createElement(
        "img",
        "yellow-elemnt",
        undefined,
        "Pizza_pictures/sous-bbq_155679418013.png",
        "sauceBBQ"
      ),
      "15"
    )
  ),
  elements.push(
    new PizzaGroup(
      createElement("div", "div2", "Рікотта"),
      createElement("div", "div3"),
      createElement("button", "btn-result"),
      createElement("button", "btn-result", "-"),
      createElement("button", "btn-result", "+"),
      createElement(
        "img",
        "yellow-elemnt",
        undefined,
        "Pizza_pictures/sous-rikotta_1556623391103.png",
        "sauceRikotta"
      ),
      "20"
    )
  );
elements.push(
  new PizzaGroup(
    createElement("div", "div2", "Сир звичайний"),
    createElement("div", "div3"),
    createElement("button", "btn-result"),
    createElement("button", "btn-result", "-"),
    createElement("button", "btn-result", "+"),
    createElement(
      "img",
      "yellow-elemnt",
      undefined,
      "Pizza_pictures/mocarela_1556623220308.png",
      "moc1"
    ),
    "20"
  )
);
elements.push(
  new PizzaGroup(
    createElement("div", "div2", "Сир фета"),
    createElement("div", "div3"),
    createElement("button", "btn-result"),
    createElement("button", "btn-result", "-"),
    createElement("button", "btn-result", "+"),
    createElement(
      "img",
      "yellow-elemnt",
      undefined,
      "Pizza_pictures/mocarela_1556785182818.png",
      "moc2"
    ),
    "25"
  )
);
elements.push(
  new PizzaGroup(
    createElement("div", "div2", "Моцарелла"),
    createElement("div", "div3"),
    createElement("button", "btn-result"),
    createElement("button", "btn-result", "-"),
    createElement("button", "btn-result", "+"),
    createElement(
      "img",
      "yellow-elemnt",
      undefined,
      "Pizza_pictures/mocarela_1556785198489.png",
      "moc3"
    ),
    "25"
  )
);
elements.push(
  new PizzaGroup(
    createElement("div", "div2", "Телятина"),
    createElement("div", "div3"),
    createElement("button", "btn-result"),
    createElement("button", "btn-result", "-"),
    createElement("button", "btn-result", "+"),
    createElement(
      "img",
      "yellow-elemnt",
      undefined,
      "Pizza_pictures/telyatina_1556624025747.png",
      "telya"
    ),
    "40"
  )
);
elements.push(
  new PizzaGroup(
    createElement("div", "div2", "Помiдори"),
    createElement("div", "div3"),
    createElement("button", "btn-result"),
    createElement("button", "btn-result", "-"),
    createElement("button", "btn-result", "+"),
    createElement(
      "img",
      "yellow-elemnt",
      undefined,
      "Pizza_pictures/vetchina.png",
      "vetch1"
    ),
    "30"
  )
);
elements.push(
  new PizzaGroup(
    createElement("div", "div2", "Гриби"),
    createElement("div", "div3"),
    createElement("button", "btn-result"),
    createElement("button", "btn-result", "-"),
    createElement("button", "btn-result", "+"),
    createElement(
      "img",
      "yellow-elemnt",
      undefined,
      "Pizza_pictures/vetchina_1556623556129.png",
      "vetch2"
    ),
    "30"
  )
);

function createBlock(elem1, elem2) {
  return elem1.append(elem2);
}

elements.forEach((item) => {
  item.button3.addEventListener("click", () => {
    item.counter++;
    item.button1.textContent = `${item.counter}`;
    ingridientsCount += parseInt(item.pissaPrice);
    resultAll[0].lastChild.textContent = `${calc()} грн`;
    a = item.img.id;
    data[a] = item.counter;
  });
});

elements.forEach((item) => {
  item.button2.addEventListener("click", () => {
    if (item.counter === 1) {
      item.div2.remove();
      item.img.remove();
      item.counter = 0;
      ingridientsSubCount -= parseInt(item.pissaPrice);
      resultAll[0].lastChild.textContent = `${calc()} грн`;
      a = item.img.id;
      data[a] = item.counter;
    } else {
      item.counter--;
      item.button1.textContent = `${item.counter}`;
      ingridientsSubCount -= parseInt(item.pissaPrice);
      resultAll[0].lastChild.textContent = `${calc()} грн`;
      a = item.img.id;
      data[a] = item.counter;
    }
  });
});

function calc(a, b, c) {
  a = parseFloat(price);
  b = parseFloat(ingridientsCount);
  c = parseFloat(ingridientsSubCount);
  return a + b + c;
}
