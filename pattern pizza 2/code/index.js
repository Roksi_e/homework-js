const [...radioBtn] = document.querySelectorAll(".radio > input, span"),
  [...resultAll] = document.querySelectorAll(".result > div"),
  pissaTable = document.querySelector(".table"),
  [...ingridients] = document.querySelectorAll(".ingridients > div > img"),
  pissaBasePrice = {
    small: "50",
    mid: "60",
    big: "90",
  };

let price = 0,
  ingridientsCount = 0,
  ingridientsSubCount = 0,
  a;

window.addEventListener("DOMContentLoaded", () => {
  resultAll.map((item) => {
    const div = createElement("div", "result-div");
    return item.appendChild(div);
  });

  radioBtn.map((item) => {
    item.addEventListener("click", (event) => {
      if (event.target.value === "small") {
        price = pissaBasePrice.small;
      }
      if (event.target.value === "mid") {
        price = pissaBasePrice.mid;
      }
      if (event.target.value === "big") {
        price = pissaBasePrice.big;
      }
      dataBase.push(event.target.value);
      return (resultAll[0].lastChild.textContent = `${calc()} грн`);
    });
  });

  ingridients.forEach((elem) => {
    elem.addEventListener(
      "dragstart",
      function (evt) {
        evt.dataTransfer.effectAllowed = "move";
        evt.dataTransfer.setData("Text", this.id);
      },
      false
    );
  });

  pissaTable.addEventListener(
    "dragover",
    function (evt) {
      if (evt.preventDefault) evt.preventDefault();
      return false;
    },
    false
  );

  pissaTable.addEventListener(
    "drop",
    function (evt) {
      if (evt.preventDefault) evt.preventDefault();
      if (evt.stopPropagation) evt.stopPropagation();
      let id = evt.dataTransfer.getData("Text"),
        elem = document.getElementById(id);

      if (elem.id === "sauceClassic") {
        elements[0].counter++;
        elements[0].createDiv();
        elements[0].img.style.zIndex = "1";
        createBlock(resultAll[1].lastElementChild, elements[0].div2);
        ingridientsCount += parseFloat(elements[0].pissaPrice);
        this.appendChild(elements[0].img);
        a = elem.id;
        data[a] = elements[0].counter;
      }
      if (elem.id === "sauceBBQ") {
        elements[1].counter++;
        elements[1].createDiv();
        elements[1].img.style.zIndex = "2";
        createBlock(resultAll[1].lastElementChild, elements[1].div2);
        ingridientsCount += parseFloat(elements[1].pissaPrice);
        this.appendChild(elements[1].img);
        a = elem.id;
        data[a] = elements[1].counter;
      }
      if (elem.id === "sauceRikotta") {
        elements[2].counter++;
        elements[2].createDiv();
        elements[2].img.style.zIndex = "3";
        createBlock(resultAll[1].lastElementChild, elements[2].div2);
        ingridientsCount += parseFloat(elements[2].pissaPrice);
        this.appendChild(elements[2].img);
        a = elem.id;
        data[a] = elements[2].counter;
      }
      if (elem.id === "moc1") {
        elements[3].counter++;
        elements[3].createDiv();
        elements[3].img.style.zIndex = "7";
        createBlock(resultAll[2].lastElementChild, elements[3].div2);
        ingridientsCount += parseFloat(elements[3].pissaPrice);
        this.appendChild(elements[3].img);
        a = elem.id;
        data[a] = elements[3].counter;
      }
      if (elem.id === "moc2") {
        elements[4].counter++;
        elements[4].createDiv();
        elements[4].img.style.zIndex = "8";
        createBlock(resultAll[2].lastElementChild, elements[4].div2);
        ingridientsCount += parseFloat(elements[4].pissaPrice);
        this.appendChild(elements[4].img);
        a = elem.id;
        data[a] = elements[4].counter;
      }
      if (elem.id === "moc3") {
        elements[5].counter++;
        elements[5].createDiv();
        elements[5].img.style.zIndex = "9";
        createBlock(resultAll[2].lastElementChild, elements[5].div2);
        ingridientsCount += parseFloat(elements[5].pissaPrice);
        this.appendChild(elements[5].img);
        a = elem.id;
        data[a] = elements[5].counter;
      }
      if (elem.id === "telya") {
        elements[6].counter++;
        elements[6].createDiv();
        elements[6].img.style.zIndex = "4";
        createBlock(resultAll[2].lastElementChild, elements[6].div2);
        ingridientsCount += parseFloat(elements[6].pissaPrice);
        this.appendChild(elements[6].img);
        a = elem.id;
        data[a] = elements[6].counter;
      }
      if (elem.id === "vetch1") {
        elements[7].counter++;
        elements[7].createDiv();
        elements[7].img.style.zIndex = "5";
        createBlock(resultAll[2].lastElementChild, elements[7].div2);
        ingridientsCount += parseFloat(elements[7].pissaPrice);
        this.appendChild(elements[7].img);
        a = elem.id;
        data[a] = elements[7].counter;
      }
      if (elem.id === "vetch2") {
        elements[8].counter++;
        elements[8].createDiv();
        elements[8].img.style.zIndex = "6";
        createBlock(resultAll[2].lastElementChild, elements[8].div2);
        ingridientsCount += parseFloat(elements[8].pissaPrice);
        this.appendChild(elements[8].img);
        a = elem.id;
        data[a] = elements[0].counter;
      }

      resultAll[0].lastChild.textContent = `${calc()} грн`;

      return false;
    },
    false
  );
});
