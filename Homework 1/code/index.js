//--Задача 1-----------------------------------

var userCity = prompt('Вкажіть своє місто'),
  userName = prompt("Вкажіть своє ім'я"),
  userAge = prompt('Вкажіть свій вік');

document.writeln('<h1>*Користувач*</h1>');
document.writeln('<p>' + userCity + '</p>');
document.writeln('<p>' + userName + '</p>');
document.writeln('<p>' + userAge + '</p>');

console.group('*Користувач*');
console.log("Ім'я: " + userName);
console.log('Вік: ' + userAge);
console.log('Місто проживання: ' + userCity);
console.groupEnd();
console.info('Кінець виводу');

//--Задача 2--------------------------------------

var x = 6,
  y = 14,
  z = 4;

var rezult1 = (x += y - x++ * z);
document.write('rezult1 = ' + rezult1 + '<br>');
/*     
x +=   приоритет 2
y-x    приоритет 15
x++    приоритет 16
*z     приоритет 13

x *z ( 6 * 4=24) y- 24(=-10 ) +=x ( -10+6) = -4
*/

var x = 6,
  y = 14,
  z = 4;

var rezult2 = (z = --x - y * 5);
document.write('rezult2 = ' + rezult2 + '<br>');
/*     
z=     приоритет 2
--x    приоритет 15
-y     приоритет 12
*5     приоритет 13

--x ( 6-1=5) y*5 ( 14*5 = 70) z= ( 5-70=-65)
*/

var x = 6,
  y = 14,
  z = 4;

var rezult3 = (y /= x + (5 % z));
document.write('rezult3 = ' + rezult3 + '<br>');
/*     
y/=    приоритет 2
x+     приоритет 12
% z    приоритет 13
*5     приоритет 13
*/

var x = 6,
  y = 14,
  z = 4;

var rezult4 = z - x++ + y * 5;
document.write('rezult4 = ' + rezult4 + '<br>');
/*     
z-x    приоритет 12
x++    приоритет 16
+y     приоритет 12
*5     приоритет 13

 x++ (6+1 = 6) 14*5 (= 70) 4-6 ( =-2) -3+70 (= 68)
*/

var x = 6,
  y = 14,
  z = 4;

var rezult5 = (x = y - x++ * z);
document.write('rezult5 = ' + rezult5 + '<br>');
/*     
x =    приоритет 2
y-x    приоритет 12
x++    приоритет 16
*z     приоритет 13

 x++ (6+1=6) 6*4 (=24) 14-24 (=-10) x=-10
*/
