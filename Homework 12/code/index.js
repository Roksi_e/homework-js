const btn = document.querySelector(".keys"),
  display = document.querySelector(".display > input"),
  screen = document.querySelector(".display"),
  numbers = [...document.querySelectorAll(".black")].map((item) => {
    return item.value;
  }),
  signs = [...document.querySelectorAll(".pink")].map((item) => {
    return item.value;
  }),
  memoryBtn = [...document.querySelectorAll(".gray")].map((item) => {
    return item.value;
  }),
  clearBtn = numbers.pop(),
  resBtn = document.querySelector(".orange"),
  calc = {
    value1: "",
    value2: "",
    sign: "",
    result: "",
    memory: "",
    isFinish: false,
  },
  span = document.createElement("span");

span.className = "memory";
//span.textContent = key;
screen.append(span);
resBtn.disabled = false;

function show(value, el) {
  el.value = value;
}

function calculate() {
  if (calc.value2 === "") {
    calc.value2 = calc.value1;
  }
  switch (calc.sign) {
    case "+":
      calc.value1 = parseFloat(calc.value1) + parseFloat(calc.value2);
      break;
    case "-":
      calc.value1 = parseFloat(calc.value1) - parseFloat(calc.value2);
      break;
    case "*":
      calc.value1 = parseFloat(calc.value1) * parseFloat(calc.value2);
      break;
    case "/":
      if (calc.value2 == 0) {
        calc.value2 = "";
        calc.sign = "";
        calc.value1 = "Error";
        return show(calc.value1, display);
      } else {
        calc.value1 = parseFloat(calc.value1) / parseFloat(calc.value2);
      }
      break;
  }
  calc.isFinish = true;
  return calc.value1;
}

function clearAll() {
  calc.value1 = "";
  calc.value2 = "";
  calc.sign = "";
  show("", display);
}

window.addEventListener("DOMContentLoaded", () => {
  btn.addEventListener("click", function (e) {
    let key = e.target.value;

    if (e.target.value === undefined) {
      return;
    }

    if (clearBtn.includes(key)) {
      show("0", display);
      span.textContent = "";
      setTimeout(clearAll, 1000);
      return;
    }

    if (numbers.includes(key)) {
      if (calc.value2 === "" && calc.sign === "") {
        calc.value1 += key;
        show(calc.value1, display);
      } else if (calc.value1 !== "" && calc.value2 !== "" && calc.isFinish) {
        calc.isFinish = false;
        calc.value2 = key;
        show(calc.value2, display);
      } else {
        calc.value2 += key;
        show(calc.value2, display);
      }
      return;
    }

    if (signs.includes(key)) {
      if (calc.value1 !== "" && calc.sign === "") {
        calc.sign = key;
        show(calc.value1, display);
        return;
      } else if (calc.value2 !== "" && calc.sign !== "") {
        calculate();
        calc.value2 = key;
        show(calc.value1, display);
      }
    }

    if (e.target.value === resBtn.value) {
      calculate();
      show(calc.value1, display);
    }

    if (memoryBtn.includes(key)) {
      if (key === "m+") {
        calc.memory = calc.value1;
        span.textContent = key;
        show("", display);
      } else if (key === "m-") {
        calc.memory = "";
        span.textContent = key;
        show("", display);
      } else if (key === "mrc") {
        if (calc.memory === "") {
          span.textContent = key;
          show("", display);
        } else if (calc.memory !== "" && calc.isFinish === false) {
          calc.isFinish = true;
          calc.memory = "";
          span.textContent = key;
          show("", display);
        } else {
          calc.isFinish = false;
          span.textContent = key;
          show(calc.memory, display);
        }
      }
    }
  });
});
