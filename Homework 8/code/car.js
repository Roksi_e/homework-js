/*
//Створити клас Car , Engine та Driver.
//Клас Driver містить поля - ПІБ, стаж водіння.
//Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
*/

class Engine {
  constructor(power, company) {
    this.power = power;
    this.company = company;
  }
  toString() {
    return `Потужність двигуна: ${this.power}, виробник: ${this.company}`;
  }
}

class Car {
  constructor(marka, carClass, weight, Engine, Driver) {
    this.marka = marka;
    this.carClass = carClass;
    this.weight = weight;
    this.engine = Engine;
    this.driver = Driver;
  }
  start() {
    return `${
      this.marka[0].toUpperCase() + this.marka.substring(1).toLowerCase()
    } - поїхали!`;
  }
  stop() {
    return `${
      this.marka[0].toUpperCase() + this.marka.substring(1).toLowerCase()
    } - зупиняємось!`;
  }
  turnRight() {
    return `${
      this.marka[0].toUpperCase() + this.marka.substring(1).toLowerCase()
    } - поворот праворуч!`;
  }
  turnLeft() {
    return `${
      this.marka[0].toUpperCase() + this.marka.substring(1).toLowerCase()
    } - поворот ліворуч!`;
  }
  toString() {
    return `Авто: ${
      this.marka[0].toUpperCase() + this.marka.substring(1).toLowerCase()
    }, класс: ${this.carClass}, вага: ${this.weight}, двигун: ${
      this.engine
    }, водій: ${this.driver}`;
  }
}

class Lorry extends Car {
  constructor(marka, carClass, weight, Engine, Driver, carrying) {
    super(marka, carClass, weight, Engine, Driver);
    this.carrying = carrying;
  }
}

class SportCar extends Car {
  constructor(marka, carClass, weight, Engine, Driver, speed) {
    super(marka, carClass, weight, Engine, Driver);
    this.speed = speed;
  }
}

const engine1 = new Engine('120 l', 'Volkswagen');
const car = new Car('mazda', 'B', 2600, engine1, driver1);
const lorry = new Lorry('bogdan', 'M', 12000, engine1, driver1, 2400);
const sportCar = new SportCar(
  'toyota',
  'F',
  1800,
  engine1,
  driver1,
  '200 km/h'
);

console.log(sportCar.start());
console.log(lorry.stop());
console.log(car.turnLeft());
console.log(lorry.turnRight());
console.log(sportCar.toString());
