class Person {
  constructor(name, surname, age) {
    this.name = name;
    this.surname = surname;
    this._age = age;
  }
  toString() {
    return `${this.name} ${this.surname}`;
  }
  get age() {
    return this._age;
  }
}

class Driver extends Person {
  constructor(name, surname, age, experience) {
    super(name, surname, age);
    this.experience = experience;
  }
}
const person = new Person('Dmytro', 'Pavlenko', 44);
const driver1 = new Driver(person.name, person.surname, person.age, 20);
