/*
## Задание

Написать реализацию кнопки "Показать пароль".

#### Технические требования:
- В файле `index.html` лежит разметка для двух полей ввода пароля. 
- По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
- Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
- Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
- По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
- Если значения совпадают - вывести модальное окно (можно alert) с текстом - `You are welcome`;
- Если значение не совпадают - вывести под вторым полем текст красного цвета  `Нужно ввести одинаковые значения`
- После нажатия на кнопку страница не должна перезагружаться
- Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.
*/
//-----------------------------------------------------------------------

let form = document.forms.password,
  span;
const [...eye] = document.querySelectorAll(".icon-password"),
  btn = document.querySelector(".btn"),
  [...input] = document.querySelectorAll("input");

eye[0].onclick = () => {
  if (eye[0].classList.contains("fa-eye") && input[0].type === "password") {
    input[0].setAttribute("type", "text");
    eye[0].classList.remove("fa-eye");
    eye[0].classList.add("fa-eye-slash");
  } else {
    input[0].setAttribute("type", "password");
    eye[0].classList.remove("fa-eye-slash");
    eye[0].classList.add("fa-eye");
  }
};

eye[1].onclick = () => {
  if (eye[1].classList.contains("fa-eye") && input[1].type === "password") {
    input[1].setAttribute("type", "text");
    eye[1].classList.remove("fa-eye");
    eye[1].classList.add("fa-eye-slash");
  } else {
    input[1].setAttribute("type", "password");
    eye[1].classList.remove("fa-eye-slash");
    eye[1].classList.add("fa-eye");
  }
};

btn.onclick = () => {
  if (form[0].value === form[1].value) {
    if (span !== undefined) {
      span.remove();
    }
    alert("You are welcom");
  } else {
    span = document.createElement("span");
    form.lastElementChild.before(span);
    span.textContent = "Нужно ввести одинаковые значения";
  }
};
