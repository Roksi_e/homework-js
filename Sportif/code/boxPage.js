const homeInput = document.querySelector(".home-input"),
  freeShop = document.querySelector(".free-shop"),
  titleTwo = document.querySelector(".two"),
  shortsPage = document.querySelector(".shorts-page");

const shortsTotal = createElement("div", "shorts-total"),
  shortsImage = createElement("div", "shorts-image"),
  shortsCard = createElement("div", "shorts-card");

let boxCart = {
  color: [],
  size: [],
};

homeInput.style.width = `60%`;

if (localStorage.productPage) {
  dataPage = JSON.parse(localStorage.productPage);
}

createProductCard(shortsCard, dataPage[1], dataPage[0]);
createImages(shortsImage, dataPage[1], dataPage[0]);

shortsCard.addEventListener("click", (e) => {
  if (e.target.className.includes("short-color-select")) {
    boxCart.color = e.target.style.backgroundColor;
  } else if (e.target.className.includes("short-size-select")) {
    boxCart.size = e.target.textContent;
  } else if (e.target.className.includes("sale-btn heart")) {
  } else if (e.target.className.includes("sale-btn")) {
    let key = dataPage[0];
    if (localStorage.customerOrder) {
      dataCart = JSON.parse(localStorage.customerOrder);
      dataPage[1].colors = boxCart.color;
      dataPage[1].sizes = boxCart.size;
      dataCart[key] = dataPage[1];
      dataCart[key].count = 1;
    } else {
      dataPage[1].colors = boxCart.color;
      dataPage[1].sizes = boxCart.size;
      dataCart[key] = dataPage[1];
      dataCart[key].count = 1;
    }
    modalTwo.style.display = "flex";
    localStorage.customerOrder = JSON.stringify(dataCart);
    checkCart();
  } else {
    return;
  }
});

titleTwo.textContent = `${dataPage[1].name}`;

const icons = [
  {
    adress: "https://www.facebook.com",
    icon: "../img/logo/Facebook.png",
  },
  {
    adress: "https://twitter.com/",
    icon: "../img/logo/Twitter.png",
  },
  {
    adress: "https://www.pinterest.com/",
    icon: "../img/logo/pinterest.png",
  },
  {
    adress: "https://rozetka.com.ua/",
    icon: "../img/logo/chain.png",
  },
];

const iconBlock = icons.map((item) => {
  let li = createElement("li");
  let a = createElement("a", undefined),
    div = createElement("div", "divv"),
    img = createElement("img");
  img.src = `${item.icon}`;
  a.href = `${item.adress}`;
  li.append(a);
  a.append(div);
  div.append(img);

  return li;
});

const iconCard = createElement("div", "div-icon"),
  iconUl = createElement("ul", "ul");
iconCard.append(iconUl);
iconUl.append(...iconBlock);
shortsCard.append(iconCard, freeShop);

checkCart();

window.addEventListener("DOMContentLoaded", () => {
  shortsPage.append(shortsTotal);
  shortsTotal.append(shortsImage, shortsCard);

  homeInputChild[3].addEventListener("click", (evt) => {
    document.location.assign("../catalog/index.html");
  });
  homeInputChild[5].textContent = `${dataPage[1].name}`;
  homeSearchInput.remove();
});
