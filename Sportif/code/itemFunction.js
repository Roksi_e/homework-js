function createProductCard(element, object, key) {
  const cardProductTitle = createElement(
      "div",
      "card-product-title",
      `${object.name} `
    ),
    cardSpan = createElement("span", "card-span", `item # ${key}`),
    cardTitle = createElement("div", "card-title"),
    cardCircle = createElement("div", "card-title"),
    circleTitle = createElement(
      "span",
      "reviews-span",
      `${object.reviews} REVIEWS`
    ),
    cardPrice = createElement("div"),
    spanPrice = createElement("div", "span-price", "As low as"),
    productPrice = createElement(
      "div",
      "sale-price",
      `$${object.price.toFixed(2)}`
    ),
    spanSize = createElement("div", "span-price", "SIZE:"),
    spanColor = createElement("div", "span-price", "COLOR:"),
    shortsProductCart = createElement("div", "div-cart-input"),
    cartOne = createElement("div", "sale-input"),
    cartTwo = createElement("div", "sale-input"),
    cartShortsOne = [
      createElement(
        "input",
        "sale-btn",
        undefined,
        undefined,
        "button",
        "ADD TO CART"
      ),
      createElement(
        "img",
        "bags",
        undefined,
        undefined,
        undefined,
        undefined,
        "../img/vector/bags.png"
      ),
    ],
    cartShortsTwo = [
      createElement(
        "input",
        "sale-btn heart",
        undefined,
        undefined,
        "button",
        "ADD TO WISHLIST"
      ),
      createElement(
        "img",
        "bags",
        undefined,
        undefined,
        undefined,
        undefined,
        "../img/vector/heart.png"
      ),
    ];

  createProductColor(spanColor, object, key);
  createProductSize(spanSize, object, key);
  createCircle(cardCircle, object);

  cartOne.append(...cartShortsOne);
  cartTwo.append(...cartShortsTwo);
  cardTitle.append(cardProductTitle, cardSpan);
  cardCircle.append(circleTitle);
  cardPrice.append(spanPrice, productPrice);
  shortsProductCart.append(cartOne, cartTwo);

  element.append(
    cardTitle,
    cardCircle,
    cardPrice,
    spanColor,
    spanSize,
    shortsProductCart
  );
  return element;
}

function clearActiveImage(array) {
  array.forEach((item) => {
    item.classList.remove("image-active");
  });
}

function createImages(element, object, key) {
  const bigImage = createElement("div", "big-images"),
    smallImage = createElement("div", "small-images"),
    image = createElement("img", "big-img");

  image.src = object.image[0];
  const images = object.image.map((item) => {
    const imageDiv = createElement("div", "small-image-div");
    const image = createElement(
      "img",
      "small-img",
      undefined,
      undefined,
      undefined,
      undefined,
      `${item}`
    );
    imageDiv.append(image);
    return imageDiv;
  });

  images[0].classList.add("image-active");

  bigImage.append(image);
  smallImage.append(...images);
  element.append(bigImage, smallImage);

  images.forEach((element) => {
    element.addEventListener("click", (e) => {
      clearActiveImage(images);
      element.classList.add("image-active");
      image.src = element.firstChild.src;
    });
  });
}
