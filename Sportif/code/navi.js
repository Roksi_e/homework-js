const blockMain = document.querySelector("main"),
  headerTop = document.querySelector("header"),
  stick = document.querySelector(".stick"),
  main = document.querySelector(".main"),
  searchImg = document.querySelector(".search > img"),
  searchInput = document.querySelector(".search > input"),
  modal = document.querySelector(".parent"),
  modalCartClose = document.querySelector(".parent span"),
  modalCartTotal = document.querySelector(".modal-cart-total"),
  cartBottom = document.querySelector(".cart-bottom"),
  priceTotal = document.querySelector("#price-total"),
  [...modalBottom] = document.querySelectorAll(".modal-bottom input");

const iconNav = document.querySelector(".icon-navi"),
  closeNav = document.querySelector(".close-navi"),
  divNav = document.querySelector(".div-nav");

if (iconNav) {
  iconNav.addEventListener("click", (e) => {
    divNav.classList.add("active-nav");
    iconNav.classList.add("active");
  });

  closeNav.addEventListener("click", (e) => {
    divNav.classList.remove("active-nav");
    iconNav.classList.remove("active");
  });
}

const dataList = createElement("datalist"),
  modalCartPoint = createElement("div", "modal-cart-point"),
  cartEmpty = createElement(
    "div",
    "cart-empty",
    "You haven`t chosen anything yet:("
  );

searchInput.setAttribute("list", "data-list");
dataList.setAttribute("id", "data-list");
searchInput.after(dataList);
divCart.append(cartCheck);

searchShot.forEach(([item, index]) => {
  const option = createElement("option");
  option.value = index.name;
  dataList.append(option);
});

let a = 0;

function showModalCart(key, { image, name, price, sizes, count }) {
  const modalCart = createElement("div", "modal-cart"),
    modalCartImg = createElement("div", "modal-cart-img"),
    cartImg = createElement("img"),
    modalCartTitle = createElement("div", "modal-cart-title"),
    modalCartSize = createElement("div", "modal-cart-size"),
    modalCartPrice = createElement("div", "modal-cart-price"),
    modalCartCount = createElement("div", "modal-cart-count"),
    btnPlus = createElement("button", undefined, "+"),
    pointCount = createElement("div"),
    btnMinus = createElement("button", undefined, "-"),
    modalPointPrice = createElement("div", "modal-cart-price"),
    btnClose = createElement("button", undefined, "X");

  btnClose.setAttribute("data-articul", key);
  btnPlus.setAttribute("data-articul", key);
  btnMinus.setAttribute("data-articul", key);

  modalCartImg.append(cartImg);
  modalCartCount.append(btnPlus, pointCount, btnMinus);
  modalCart.append(
    modalCartImg,
    modalCartTitle,
    modalCartSize,
    modalCartPrice,
    modalCartCount,
    modalPointPrice,
    btnClose
  );

  cartImg.src = `${image[0]}`;
  modalCartTitle.textContent = `${name}`;
  modalCartSize.textContent = `${sizes}`;
  modalCartPrice.textContent = `${price.toFixed(2)}`;
  pointCount.textContent = `${count}`;
  modalPointPrice.textContent = `${parseFloat(price * count).toFixed(2)}`;

  btnPlus.addEventListener("click", (e) => {
    if (e.target.dataset.articul === key) {
      dataCart[key].count++;
      localStorage.customerOrder = JSON.stringify(dataCart);
    }
    count++;
    pointCount.textContent = `${count}`;
    modalPointPrice.textContent = `${parseFloat(price * count).toFixed(2)}`;
    showPrice(dataCart);
  });

  btnMinus.addEventListener("click", (e) => {
    if (e.target.dataset.articul === key) {
      dataCart[key].count--;
      localStorage.customerOrder = JSON.stringify(dataCart);
      if (dataCart[key].count < 1) {
        delete dataCart[key];
        localStorage.customerOrder = JSON.stringify(dataCart);
      }
    }
    if (count <= 1) {
      modalCart.remove();
    }
    count--;
    pointCount.textContent = `${count}`;
    modalPointPrice.textContent = `${parseFloat(price * count).toFixed(2)}`;
    showPrice(dataCart);
  });

  btnClose.addEventListener("click", (e) => {
    modalCart.remove();
    if (e.target.dataset.articul === key) {
      delete dataCart[key];
      localStorage.customerOrder = JSON.stringify(dataCart);
      checkCart();
      showPrice(dataCart);
    }
    if (Object.keys(dataCart).length === 0) {
      modal.classList.add("active-modal");
      cartBottom.style.display = "none";
      document.querySelector(".parent > .modal").append(cartEmpty);
      checkCart();
    }
  });

  return modalCartPoint.append(modalCart);
}

function viewCart(object) {
  for (let key in object) {
    showModalCart(key, object[key]);
  }
}

function showPrice(object) {
  let priceAll = 0;
  for (let key in object) {
    priceAll += parseFloat(object[key].price * object[key].count);
  }
  priceTotal.value = priceAll.toFixed(2);
}

window.addEventListener("DOMContentLoaded", () => {
  main.prepend(modal, modalTwo);

  divCart.addEventListener("click", (e) => {
    if (localStorage.customerOrder) {
      dataCart = JSON.parse(localStorage.customerOrder);
      viewCart(dataCart);
      showPrice(dataCart);
      modal.classList.add("active-modal");
      cartEmpty.remove();
      modalCartTotal.append(modalCartPoint);
      cartBottom.style.display = "flex";
    }
    if (!localStorage.customerOrder || Object.keys(dataCart).length === 0) {
      modal.classList.add("active-modal");
      cartBottom.style.display = "none";
      document.querySelector(".parent > .modal").append(cartEmpty);
    }
  });

  modalCartClose.addEventListener("click", (e) => {
    modalCartPoint.innerHTML = "";
    modal.classList.remove("active-modal");
  });

  modalBottom.forEach((el) => {
    el.addEventListener("click", (e) => {
      if (e.target.value === "Continue shopping") {
        modal.classList.remove("active-modal");
        modalCartPoint.remove();
      }
      if (e.target.value === "Checkout") {
        modal.classList.remove("active-modal");
        window.open("https://next.privat24.ua/");
      }
    });
  });

  searchImg.addEventListener("click", (evt) => {
    dataPage = userShowProduct.find((element) => {
      return element[1].name === searchInput.value;
    });
    localStorage.productPage = JSON.stringify(dataPage);
    document.location.assign("../shorts/index.html");
  });
});
