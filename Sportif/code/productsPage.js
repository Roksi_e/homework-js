const containerShorts = document.querySelector(".container-shorts");

const iconSearchBar = document.querySelector(".icon-search-bar"),
  closeSearchBar = document.querySelector(".close-search-bar");

let productsList = createElement("div", "product-list"),
  shortsProductList = [
    createElement("div", "product-grid"),
    createElement("div", "product-grid"),
    createElement("div", "product-grid"),
    createElement("div", "product-grid"),
    createElement("div", "product-grid"),
  ];

const bottomMain = createElement("div", "bottom-main"),
  bottomBtn = [
    createElement("bottom", "btn-bottom-main active", "1"),
    createElement("bottom", "btn-bottom-main ", "2"),
    createElement("bottom", "btn-bottom-main ", "3"),
    createElement("bottom", "btn-bottom-main ", "4"),
    createElement("bottom", "btn-bottom-main ", "5"),
  ],
  btnRight = createElement("bottom", "btn-bottom-main base"),
  btnRightImg = createElement("img");

btnRightImg.src = "../img/vector/right-bordo.png";
btnRight.append(btnRightImg);

const searchBarTotal = createElement("div", "search-bar-total"),
  searchBar = createElement("div", "search-bar"),
  resetBtn = createElement("button", "reset-btn", "Reset all filters");

for (let i = 0; i <= 6; i++) {
  i = selectBar.push(
    new SelectionBar(
      createElement("div", "head-select"),
      createElement("div", "div-head-select"),
      createElement("div", "div-vector-select"),
      createElement("img", "img-select"),
      createElement("div", "body-select")
    )
  );
}

selectBar[0].divHeadSelect.textContent = "SIZE";
selectBar[1].divHeadSelect.textContent = "COLOR";
selectBar[2].divHeadSelect.textContent = "BOTTOMS";
selectBar[3].divHeadSelect.textContent = "INSEAM";
selectBar[4].divHeadSelect.textContent = "SUSTAINABLE FABRIC";
selectBar[5].divHeadSelect.textContent = "COLLECTIONS";

selectBar.map((item) => {
  item.vectorSelect.src = "../img/vector/down.png";
  return searchBar.append(item.createHeadSelectBar());
});

selectBar.forEach((item) => {
  item.divVectorSelect.addEventListener("click", (evt) => {
    if (item.isVector) {
      item.headSelect.after(item.bodySelect);
      item.vectorSelect.src = "../img/vector/up.png";
      item.isVector = false;
    } else {
      item.vectorSelect.src = "../img/vector/down.png";
      item.isVector = true;
      item.bodySelect.remove();
    }
  });
});

sizeSelect.map((item) => {
  let div = createElement("div", "size-select", `${item}`);
  selectBar[0].bodySelect.append(div);
  div.addEventListener("click", (evt) => {
    clearPage(shortsProductList);
    userShowProduct = sizeFilter(
      userShowProduct,
      parseInt(evt.target.textContent)
    );
    changeProductList(userShowProduct, shortsProductList);
  });
  return div;
});

colorSelect.map((item) => {
  let div = createElement("div", "color-select");
  div.style.backgroundColor = `${item}`;
  selectBar[1].bodySelect.append(div);
  div.addEventListener("click", (evt) => {
    userShowProduct = colorFilter(userShowProduct, item);
    clearPage(shortsProductList);
    changeProductList(userShowProduct, shortsProductList);
  });

  return div;
});

bottomsSelect.map((item) => {
  let div = createElement("div", "bottoms-select"),
    input = createElement(
      "input",
      undefined,
      undefined,
      undefined,
      "checkbox",
      `${item}`
    ),
    label = createElement("label", undefined, `${item}`);
  input.id = `${item}`;
  label.setAttribute("for", `${item}`);
  div.append(input, label);
  selectBar[2].bodySelect.append(div);
  div.addEventListener("click", (evt) => {
    if (
      evt.target.value === undefined ||
      evt.target.value === null ||
      evt.target.value === ""
    )
      return;
  });
  return div;
});

function changeProductList(array, ar) {
  array.forEach((item, index) => {
    if (index >= 0 && index <= 9) {
      ar[0].append(showProduct(item));
    } else if (index >= 10 && index <= 19) {
      ar[1].append(showProduct(item));
    } else if (index >= 20 && index <= 29) {
      ar[2].append(showProduct(item));
    } else if (index >= 30 && index <= 39) {
      ar[3].append(showProduct(item));
    } else if (index >= 40 && index <= 49) {
      ar[4].append(showProduct(item));
    }
  });
}

function changePage(array) {
  bottomBtn.forEach((item, index) => {
    item.addEventListener("click", (e) => {
      clearActiveBtn(bottomBtn);
      item.classList.add("active");
      if (index === 0) {
        removeList(array);
        productsList.append(array[0]);
      } else if (index === 1) {
        removeList(array);
        productsList.append(array[1]);
      } else if (index === 2) {
        removeList(array);
        productsList.append(array[2]);
      } else if (index === 3) {
        removeList(array);
        productsList.append(array[3]);
      } else if (index === 4) {
        removeList(array);
        productsList.append(array[4]);
      }
    });
  });
}

if (iconSearchBar) {
  iconSearchBar.addEventListener("click", (e) => {
    searchBar.classList.add("active-bar");
    closeSearchBar.classList.add("active-close-bar");
    iconSearchBar.classList.add("active-search-bar");
  });

  closeSearchBar.addEventListener("click", (e) => {
    searchBar.classList.remove("active-bar");
    closeSearchBar.classList.remove("active-close-bar");
    iconSearchBar.classList.remove("active-search-bar");
  });
}

changeProductList(userShowProduct, shortsProductList);

checkCart();

window.addEventListener("DOMContentLoaded", () => {
  searchBarTotal.append(iconSearchBar, searchBar, closeSearchBar);
  containerShorts.append(searchBarTotal, productsList);
  productsList.append(shortsProductList[0]);
  containerShorts.after(bottomMain);
  bottomMain.append(...bottomBtn, btnRight);
  searchBar.insertAdjacentElement("beforeend", resetBtn);

  resetBtn.addEventListener("click", (e) => {
    clearPage(shortsProductList);
    userShowProduct = searchShot;
    changeProductList(userShowProduct, shortsProductList);
  });

  changePage(shortsProductList);

  let i = 1,
    j = 1;

  btnRight.addEventListener("click", (e) => {
    clearActiveBtn(bottomBtn);
    bottomBtn[i].classList.add("active");
    i++;
    removeList(shortsProductList);
    productsList.append(shortsProductList[j]);
    j++;
    if (i >= bottomBtn.length) {
      i = 0;
      j = 0;
    }
  });
});
