const homeHeader = document.querySelector(".home-header"),
  dataListSearch = createElement("datalist"),
  homeSearchInput = document.querySelector(".home-search-input"),
  [...homeInputChild] = document.querySelectorAll(".home-input > button");

homeSearchInput.setAttribute("list", "data-list-shorts");
dataListSearch.setAttribute("id", "data-list-shorts");

homeSearchInput.after(dataListSearch);

searchShot.forEach(([item, index]) => {
  const option = createElement("option");
  option.value = index.name;
  dataListSearch.append(option);
});

window.addEventListener("DOMContentLoaded", () => {
  homeInputChild[1].addEventListener("click", (evt) => {
    document.location.assign("../index.html");
  });

  homeSearchInput.addEventListener("change", (evt) => {
    dataPage = userShowProduct.find((element) => {
      return element[1].name === homeSearchInput.value;
    });
    localStorage.productPage = JSON.stringify(dataPage);
    document.location.assign("../shorts/index.html");
  });
});
