const musics = ['Джаз', 'Блюз'];
document.write(`<p>Начальний масив: ${musics}`);
musics.push('Рок-н-ролл');
document.write(`<p>Додавання елементу в кінець масиву: ${musics.join('*')}`);
musics[Math.floor((musics.length - 1) / 2)] = 'Классика';
document.write(`<p>Заміна елементу в середині масиву: ${musics.join('#')}`);
let firstElement = musics.shift();
document.write(`<p>Вилучений елемент: ${firstElement}`);
document.write(
  `<p>Масив після вилучення першого елементу: ${musics.join('^')}`
);
musics.unshift('Рэп', 'Регги');
document.write(
  `<p>Додавання елементів на початок масиву: ${musics.join('&#10059')}`
);
