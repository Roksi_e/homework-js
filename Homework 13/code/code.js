import data from "./koatuu.js";

window.addEventListener("DOMContentLoaded", () => {
  // 1.
  // Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test"

  const [...input] = document.querySelectorAll(".input");

  function change(e) {
    document.querySelector("#text").innerText = e.target.value;
  }

  input.forEach((element) => {
    element.addEventListener("change", change);
  });

  //   2.
  // Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів.
  // Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість,
  // то межа інпуту стає зеленою, якщо неправильна – червоною.

  const [...inputBlur] = document.querySelectorAll(".blur");

  function blur(e) {
    const getInput = e.target.dataset.length,
      setInput = e.target.value.length;

    if (setInput >= getInput) {
      e.target.style.borderColor = "green";
    } else {
      e.target.style.borderColor = "red";
    }
  }

  inputBlur.forEach((element) => {
    element.addEventListener("blur", blur);
  });

  //   3.
  // Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача данні.
  // Ім'я, Прізвище (Українською)
  // Список з містами України
  // Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора.
  // Пошта
  // Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅

  // Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані

  const [...inputIcon] = document.querySelectorAll(".input-icon"),
    inputCity = document.querySelector("#city"),
    btnSend = document.querySelector("#icon-send");

  const user = {
      name: "",
      lastName: "",
      city: "",
      phone: "",
      email: "",
    },
    img = document.createElement("img");

  data.forEach((element) => {
    const option = document.createElement("option");
    option.value = element["Назва об'єкта українською мовою"];
    document.querySelector("#data-city").append(option);
  });

  function validate(pattern, value) {
    return pattern.test(value);
  }

  function checkPos(element) {
    if (element.nextSibling !== null) {
      element.nextSibling.remove();
      element.after("✅");
    } else {
      element.after("✅");
    }
  }

  function chechNeg(element) {
    if (element.nextSibling !== null) {
      element.nextSibling.remove();
      element.after("❌");
    } else {
      element.after("❌");
    }
  }

  inputIcon.forEach((el) => {
    el.addEventListener("change", (e) => {
      if (el.id === "name") {
        if (validate(/^[А-яЇїЄєІіҐґ-]+$/i, e.target.value)) {
          checkPos(e.target);
          user.name = e.target.value.toLowerCase();
        } else {
          chechNeg(e.target);
        }
      }
      if (el.id === "lastname") {
        if (validate(/^[А-яЇїЄєІіҐґ-]+$/i, e.target.value)) {
          checkPos(e.target);
          user.lastName = e.target.value.toLowerCase();
        } else {
          chechNeg(e.target);
        }
      }
      if (el.id === "tel") {
        if (validate(/\+38\d{3}\ \d{3} \d{2} \d{2}/, e.target.value)) {
          checkPos(e.target);
          user.phone = e.target.value.toLowerCase();
          const mobileOperator = /[0]\d{2}/.exec(e.target.value);
          e.target.nextSibling.after(img);
          if (
            mobileOperator[0] === "050" ||
            mobileOperator[0] === "095" ||
            mobileOperator[0] === "066" ||
            mobileOperator[0] === "099"
          ) {
            img.src = "./img/Vodafone_icon.png";
          }
          if (
            mobileOperator[0] === "039" ||
            mobileOperator[0] === "067" ||
            mobileOperator[0] === "068" ||
            mobileOperator[0] === "096" ||
            mobileOperator[0] === "097" ||
            mobileOperator[0] === "098"
          ) {
            img.src = "./img/kyivstar.png";
          }
          if (
            mobileOperator[0] === "063" ||
            mobileOperator[0] === "093" ||
            mobileOperator[0] === "073"
          ) {
            img.src = "./img/Lifecell.png";
          }
        } else {
          chechNeg(e.target);
        }
      }
      if (el.id === "email") {
        if (
          validate(/\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i, e.target.value)
        ) {
          checkPos(e.target);
          user.email = e.target.value.toLowerCase();
        } else {
          chechNeg(e.target);
        }
      }
    });
  });

  btnSend.addEventListener("click", (e) => {
    if (
      inputCity.value !== "" &&
      inputIcon[0].nextSibling.nodeValue === "✅" &&
      inputIcon[1].nextSibling.nodeValue === "✅" &&
      inputIcon[2].nextSibling.nodeValue === "✅" &&
      inputIcon[3].nextSibling.nodeValue === "✅"
    ) {
      user.city = inputCity.value.toLowerCase();
      console.dir(user);
    } else {
      alert("Please, complete all forms");
    }
  });

  /*
4.
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
 `Поточна ціна: ${значення з поля введення}`.  
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

  const inputNumber = document.querySelector("#input-number"),
    divNumber = document.querySelector(".div-number"),
    dataSpan = {
      span: document.createElement("span"),
      btn: document.createElement("button"),
    };

  function removeClass(element) {
    element.classList.remove("green");
    element.classList.remove("red");
    element.classList.remove("focus");
  }

  inputNumber.addEventListener("focus", (e) => {
    removeClass(e.target);
    e.target.classList.add("green");
  });

  inputNumber.addEventListener("blur", (e) => {
    if (e.target.value > 0) {
      removeClass(e.target);
      e.target.classList.add("focus");
      dataSpan.span.innerText = `Поточна ціна: ${e.target.value}`;
      dataSpan.btn.innerText = "X";
      divNumber.before(dataSpan.span, dataSpan.btn);
      divNumber.nextSibling.nodeValue = "";
    } else {
      removeClass(e.target);
      e.target.classList.add("red");
      if (divNumber.nextSibling !== "") {
        divNumber.nextSibling.remove();
        divNumber.insertAdjacentText("afterend", `Please enter correct price`);
      }
    }
  });

  dataSpan.btn.addEventListener("click", (e) => {
    dataSpan.span.remove();
    dataSpan.btn.remove();
    removeClass(inputNumber);
    inputNumber.value = "";
  });
});
