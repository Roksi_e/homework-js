/*
Створіть слайдер кожні 3 сек змінюватиме зображення
Зображення для відображення
*/

const planets = [
  "https://images.unsplash.com/photo-1614732414444-096e5f1122d5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGxhbmV0fGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
  "https://images.unsplash.com/photo-1630839437035-dac17da580d0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fHBsYW5ldHxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60",
  "https://images.unsplash.com/photo-1632395627732-005012dbc286?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80",
  "https://images.unsplash.com/photo-1614313913007-2b4ae8ce32d6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80",
  "https://images.unsplash.com/photo-1614730321146-b6fa6a46bcb4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8cGxhbmV0fGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
];

const main = document.querySelector("#root"),
  container = document.createElement("div"),
  img = document.createElement("img");

let i = 0;

img.setAttribute(
  "src",
  "https://images.unsplash.com/photo-1614730321146-b6fa6a46bcb4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8cGxhbmV0fGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60"
);
main.style.width = `${window.innerWidth}px`;
main.style.height = `${window.innerHeight}px`;
changeOpacity();
container.classList.add("container");

const changePlanet = () => {
  img.src = planets[i];
  i++;
  if (i >= planets.length) {
    i = 0;
  }
};
function changeOpacity() {
  if (main.className === "") {
    main.classList.add("animate");
  }
  setTimeout(() => {
    if (main.className !== "") {
      main.classList.remove("animate");
    }
  }, 2980);
}

window.onload = () => {
  main.append(container);
  container.append(img);
  setInterval(changePlanet, 3000);
  setInterval(changeOpacity, 3000);
};
