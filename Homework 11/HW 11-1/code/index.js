const stopwatch = document.querySelector(".container-stopwatch");
const get = (id) => {
  return document.getElementById(id);
};
let second = 0,
  minute = 0,
  hour = 0,
  interval;

get("start").onclick = () => {
  clearClass();
  stopwatch.classList.add("green");
  clearInterval(interval);
  interval = setInterval(() => {
    second++;
    if (second > 59) {
      second = 0;
      minute++;
      if (minute > 59) {
        minute = 0;
        hour++;
      }
    }
    showInterval();
  }, 1000);
};

get("stop").onclick = () => {
  clearClass();
  stopwatch.classList.add("red");
  clearInterval(interval);
};

get("reset").onclick = () => {
  clearClass();
  stopwatch.classList.add("silver");
  clearInterval(interval);
  resetTime();
};
function clearClass() {
  stopwatch.classList.remove("red");
  stopwatch.classList.remove("black");
  stopwatch.classList.remove("green");
  stopwatch.classList.remove("silver");
}

function showInterval() {
  if (second < 10) {
    get("sec").innerText = `0${second}`;
  } else if (second >= 10 && second < 60) {
    get("sec").innerText = `${second}`;
  }
  if (minute < 10) {
    get("minute").innerText = `0${minute}`;
  } else if (minute >= 10 && minute < 60) {
    get("minute").innerText = `${minute}`;
  }
  if (hour < 10) {
    get("hours").innerText = `0${hour}`;
  } else if (hour >= 10) {
    get("hours").innerText = `${hour}`;
  }
}
function resetTime() {
  second = 0;
  minute = 0;
  hour = 0;
  get("sec").innerText = `00`;
  get("minute").innerText = `00`;
  get("hours").innerText = `00`;
}
