/*
Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, 
якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача 
на сторінку https://bel.biz/wp-content/uploads/2020/01/vipy.tv_-e1578133224386.jpg
якщо буде помилка, відобразіть її в діві до input.
*/

function createElement(
  elementName,
  className = "",
  content = "",
  type = "",
  placeholder = "",
  value = ""
) {
  const element = document.createElement(elementName);
  if (className !== "" || className !== null) {
    element.className = className;
  }
  if (type !== "" || type !== null) {
    element.type = type;
  }
  if (placeholder !== "" || placeholder !== null) {
    element.placeholder = placeholder;
  }
  if (value !== "" || value !== null) {
    element.value = value;
  }
  if (content !== "" || content !== null) {
    element.textContent = content;
  }
  return element;
}

const input = createElement(
    "input",
    "class-input",
    undefined,
    "phone",
    "000-000-00-00"
  ),
  button = createElement(
    "input",
    "class-button",
    undefined,
    "button",
    undefined,
    "Зберегти"
  );

let pattern = /\d{3}-\d{3}-\d{2}-\d{2}/,
  phoneCheck,
  div;

window.onload = () => {
  document.body.prepend(button);
  document.body.prepend(input);
  button.onclick = () => {
    if (pattern.test(input.value)) {
      input.style.backgroundColor = "green";
      setTimeout(() => {
        document.location =
          "https://bel.biz/wp-content/uploads/2020/01/vipy.tv_-e1578133224386.jpg";
      }, 2000);
      if (div !== "" || div !== null) {
        div.style.display = "none";
      }
    } else {
      div = createElement(
        "div",
        "class-div",
        `Ви ввели невірний номер ${input.value}. Введіть номер відповідно до формату "000-000-00-00`
      );
      button.after(div);
      input.value = "";
    }
  };
};
