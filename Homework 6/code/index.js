// Напишіть функцію isEmpty(obj), яка повертає true, якщо об'єкт не має властивостей, інакше false.

function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}

const obj1 = {};
const obj2 = {
  x: 1,
  y: 2,
};

console.log(isEmpty(obj1));
console.log(isEmpty(obj2));

//------------------------------------------------------
//  Розробіть функцію-конструктор, яка буде створювати об'єкт Human. Створіть масив об'єктів та реалізуйте функцію, яка буде сортувати масив по значенню властивості Age

function Human(name, surname, age) {
  this.name = name;
  this.surname = surname;
  this.age = age;
}

let humans = [];
humans.push(new Human('Ivan', 'Ivanov', '22'));
humans.push(new Human('Margo', 'Saab', '18'));
humans.push(new Human('Dmytro', 'Kovin', '29'));
humans.push(new Human('Pavlo', 'Stolair', '32'));
humans.push(new Human('Daria', 'Luchko', '30'));

function compareAge(array) {
  return array.sort((a, b) => a.age - b.age);
}

console.log(compareAge(humans));

//--------------------------------------------------------------------------------------
// . Розробіть функцію-конструктор, яка буде створювати об'єкт Human, додайте властивості та методи в цей об'єкт. Зробіть на рівні екземпляра та на рівні функціі-конструктора

function Humans(name, surname, age, status) {
  this.name = name;
  this.surname = surname;
  this.age = age;
  this.status = status;
  this.add = function () {
    if (
      Humans.instances === NaN ||
      Humans.instances === undefined ||
      Humans.instances >= 3
    ) {
      Humans.instances = 0;
    }
    Humans.instances++;
    if (Humans.instances === 3) {
      alert('Вітаю! Ваша знижка 5%');
    }
    console.log(Humans.instances);
    return Humans.instances;
  };
  this.add();
}
const btn = document.getElementById('btn');
let human = {};
const humanAll = [];

Humans.prototype.humanCard = function () {
  document.getElementById('root').innerHTML += `
  <div class="card">
  <p>Ім'я: <em><b>${this.name}</b></em></p>
  <p>Прізвище: <em><b>${this.surname}</b></em></p>
  <p>Вік: <em><b>${this.age}</b></em></p>
  <p>Візит: <em><b>${this.status}</b></em></p>
</div>
  `;
};

function btnClick(array) {
  human = new Humans(
    prompt("Введіть ім'я"),
    prompt('Ввудіть призвіще'),
    prompt('Введіть вік'),
    prompt('Первинний візит - введіть 1, інакше - 2', '1 або 2')
  );
  Humans.countCheck;
  human.humanCard();

  return array.push(human);
}

btn.onclick = () => {
  btnClick(humanAll);
};

console.log(humanAll);
