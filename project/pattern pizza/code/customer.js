const [...customerForm] = document.info,
  customerInfo = customerForm.filter((elem) => {
    return elem.className !== "button";
  });

let res;
if (sessionStorage.customerOrder) {
  dataAll = JSON.parse(sessionStorage.customerOrder);
  console.log(dataAll);
}
customerInfo.forEach((el) => {
  el.addEventListener("change", (event) => {
    validate(event.target);
    if (res === true) {
      event.target.classList.add("success");
      event.target.classList.remove("error");
    } else {
      event.target.classList.add("error");
      event.target.classList.remove("success");
    }
  });
});

function validate(target) {
  switch (target.type) {
    case "text":
      return (res = /^[A-zА-яієї]{2,}$/i.test(target.value));
    case "tel":
      return (res = /^\+380\d{9}$/.test(target.value));
    case "email":
      return (res = /\b[A-z0-9.-_]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i.test(
        target.value
      ));
  }
}

customerForm[3].addEventListener("click", (event) => {
  let res1 = confirm("Ви впевненні?");
  if (res1 === true) {
    customerInfo.map((el) => {
      el.target = "";
      el.classList.remove("error");
      el.classList.remove("success");
    });
  } else {
    event.preventDefault();
  }
});

customerForm[4].addEventListener("click", () => {
  if (
    customerInfo[0].className !== "error" &&
    customerInfo[1].className !== "error" &&
    customerInfo[2].className !== "error" &&
    customerInfo[0].className !== "" &&
    customerInfo[1].className !== "" &&
    customerInfo[2].className !== ""
  ) {
    data.push(
      resultAll[0].lastChild.textContent,
      dataNew,
      new CustomerCard(
        customerInfo[0].value,
        customerInfo[1].value,
        customerInfo[2].value
      )
    );
    sessionStorage.customerOrder = JSON.stringify(data);
    document.location.assign("./thank-you.html");
  } else if (
    customerInfo[0].className === "error" ||
    customerInfo[1].className === "error" ||
    customerInfo[2].className === "error"
  ) {
    alert("Заповніть, будь ласка, форму правильно");
  } else {
    alert("Заповніть форму");
  }
});
