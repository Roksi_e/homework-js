window.addEventListener("DOMContentLoaded", () => {});
const [...radioBtn] = document.querySelectorAll(".radio > input, span"),
  [...resultAll] = document.querySelectorAll(".result > div"),
  pissaTable = document.querySelector(".table"),
  [...ingridients] = document.querySelectorAll(".ingridients > div > img");

let price = 0,
  ingridientsCount = 0,
  ingridientsSubCount = 0;

resultAll.map((item) => {
  const div = createElement("div", "result-div");
  return item.appendChild(div);
});

radioBtn.map((item) => {
  item.addEventListener("click", (event) => {
    if (event.target.value === "small") {
      price = pissaPrice.small;
    }
    if (event.target.value === "mid") {
      price = pissaPrice.mid;
    }
    if (event.target.value === "big") {
      price = pissaPrice.big;
    }
    dataNew.push(event.target.value);
    return (resultAll[0].lastChild.textContent = `${calc()} грн`);
  });
});

ingridients.forEach((elem) => {
  elem.addEventListener(
    "dragstart",
    function (evt) {
      evt.dataTransfer.effectAllowed = "move";
      evt.dataTransfer.setData("Text", this.id);
    },
    false
  );
});

pissaTable.addEventListener(
  "dragover",
  function (evt) {
    if (evt.preventDefault) evt.preventDefault();
    return false;
  },
  false
);

pissaTable.addEventListener(
  "drop",
  function (evt) {
    if (evt.preventDefault) evt.preventDefault();
    if (evt.stopPropagation) evt.stopPropagation();
    let id = evt.dataTransfer.getData("Text"),
      elem = document.getElementById(id),
      img = createElement("img", "yellow-elemnt"),
      divRes = createElement("div", "ingrid-block"),
      span = createElement("span", "res-span"),
      button = createElement("button", "res-button", "x");
    divRes.append(span, button);

    if (elem.id === "sauceClassic") {
      img.src = "Pizza_pictures/sous-klassicheskij_1557758736353.png";
      img.style.zIndex = "1";
      ingridientsCount += parseFloat(pissaPrice.sauceClassic);
      span.textContent = "Кетчуп";
      resultAll[1].lastElementChild.append(divRes);
      button.addEventListener("click", () => {
        deleteElement(divRes, img, pissaPrice.sauceClassic);
        resultAll[0].lastChild.textContent = `${calc()} грн`;
        dataNew.push(new DataMinus(span.textContent, ingridientsSubCount));
      });
    }
    if (elem.id === "sauceBBQ") {
      img.src = "Pizza_pictures/sous-bbq_155679418013.png";
      img.style.zIndex = "2";
      ingridientsCount += parseFloat(pissaPrice.sauceBBQ);
      span.textContent = "Соус BBQ";
      resultAll[1].lastElementChild.append(divRes);
      button.addEventListener("click", () => {
        deleteElement(divRes, img, pissaPrice.sauceBBQ);
        resultAll[0].lastChild.textContent = `${calc()} грн`;
        dataNew.push(new DataMinus(span.textContent, ingridientsSubCount));
      });
    }
    if (elem.id === "sauceRikotta") {
      img.src = "Pizza_pictures/sous-rikotta_1556623391103.png";
      img.style.zIndex = "3";
      ingridientsCount += parseFloat(pissaPrice.sauceRikotta);
      span.textContent = "Рікотта";
      resultAll[1].lastElementChild.append(divRes);
      button.addEventListener("click", () => {
        deleteElement(divRes, img, pissaPrice.sauceRikotta);
        resultAll[0].lastChild.textContent = `${calc()} грн`;
        dataNew.push(new DataMinus(span.textContent, ingridientsSubCount));
      });
    }
    if (elem.id === "moc1") {
      img.src = "Pizza_pictures/mocarela_1556623220308.png";
      img.style.zIndex = "9";
      ingridientsCount += parseFloat(pissaPrice.cheese);
      span.textContent = "Сир";
      resultAll[2].lastElementChild.append(divRes);
      button.addEventListener("click", () => {
        deleteElement(divRes, img, pissaPrice.cheese);
        resultAll[0].lastChild.textContent = `${calc()} грн`;
        dataNew.push(new DataMinus(span.textContent, ingridientsSubCount));
      });
    }
    if (elem.id === "moc2") {
      img.src = "Pizza_pictures/mocarela_1556785182818.png";
      img.style.zIndex = "5";
      ingridientsCount += parseFloat(pissaPrice.feta);
      span.textContent = "Фета";
      resultAll[2].lastElementChild.append(divRes);
      button.addEventListener("click", () => {
        deleteElement(divRes, img, pissaPrice.feta);
        resultAll[0].lastChild.textContent = `${calc()} грн`;
        dataNew.push(new DataMinus(span.textContent, ingridientsSubCount));
      });
    }
    if (elem.id === "moc3") {
      img.src = "Pizza_pictures/mocarela_1556785198489.png";
      img.style.zIndex = "4";
      ingridientsCount += parseFloat(pissaPrice.mocarela);
      span.textContent = "Моцарела";
      resultAll[2].lastElementChild.append(divRes);
      button.addEventListener("click", () => {
        deleteElement(divRes, img, pissaPrice.mocarela);
        resultAll[0].lastChild.textContent = `${calc()} грн`;
        dataNew.push(new DataMinus(span.textContent, ingridientsSubCount));
      });
    }
    if (elem.id === "telya") {
      img.src = "Pizza_pictures/telyatina_1556624025747.png";
      img.style.zIndex = "6";
      ingridientsCount += parseFloat(pissaPrice.beef);
      span.textContent = "Телятина";
      resultAll[2].lastElementChild.append(divRes);
      button.addEventListener("click", () => {
        deleteElement(divRes, img, pissaPrice.beef);
        resultAll[0].lastChild.textContent = `${calc()} грн`;
        dataNew.push(new DataMinus(span.textContent, ingridientsSubCount));
      });
    }
    if (elem.id === "vetch1") {
      img.src = "Pizza_pictures/vetchina.png";
      img.style.zIndex = "7";
      ingridientsCount += parseFloat(pissaPrice.tomato);
      span.textContent = "Помідори";
      resultAll[2].lastElementChild.append(divRes);
      button.addEventListener("click", () => {
        deleteElement(divRes, img, pissaPrice.tomato);
        resultAll[0].lastChild.textContent = `${calc()} грн`;
        dataNew.push(new DataMinus(span.textContent, ingridientsSubCount));
      });
    }
    if (elem.id === "vetch2") {
      img.src = "Pizza_pictures/vetchina_1556623556129.png";
      img.style.zIndex = "8";
      ingridientsCount += parseFloat(pissaPrice.mushrooms);
      span.textContent = "Печериці";
      resultAll[2].lastElementChild.append(divRes);
      button.addEventListener("click", () => {
        deleteElement(divRes, img, pissaPrice.mushrooms);
        resultAll[0].lastChild.textContent = `${calc()} грн`;
        dataNew.push(new DataMinus(span.textContent, ingridientsSubCount));
      });
    }

    resultAll[0].lastChild.textContent = `${calc()} грн`;

    dataNew.push(span.textContent);

    this.appendChild(img);

    return false;
  },
  false
);
