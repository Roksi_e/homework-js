const cartBox = document.querySelector(".cart-box"),
  cartPriceTotal = document.querySelector(".cart-price_total");

dataCart = JSON.parse(localStorage.customerOrder);
if (sessionStorage.productBase) {
  dataBase = JSON.parse(sessionStorage.productBase);
}

function createCart(obj) {
  const cart = createElement("div", "cart-item"),
    cartImage = createElement("div", "cart-img"),
    img = createElement("img", undefined, undefined, obj.image),
    p = createElement("p", "cart-name", obj.name),
    cartCountTotal = createElement("div", "cart-count_total mx-5"),
    spanMinus = createElement("span", "span-count", "-"),
    spanCount = createElement("span", undefined, obj.count),
    spanPlus = createElement("span", "span-count", "+"),
    cartPrice = createElement(
      "div",
      "cart-price",
      `${(obj.price * obj.count).toFixed(2)}`
    );

  img.alt = obj.name;

  cartImage.append(img);
  cartCountTotal.append(spanMinus, spanCount, spanPlus);
  cart.append(cartImage, p, cartCountTotal, cartPrice);

  spanMinus.addEventListener("click", (e) => {
    dataBase.forEach((el) => {
      if (el.id === obj.id) {
        el.count--;
        sessionStorage.productBase = JSON.stringify(dataBase);
        if (el.count === 0) {
          return;
        }
      }
    });

    Object.keys(dataCart).forEach((el) => {
      if (el === obj.id) {
        dataCart[el].count--;
        spanCount.textContent = dataCart[el].count;
        cartPrice.textContent = `${(
          dataCart[el].price * dataCart[el].count
        ).toFixed(2)}`;
        localStorage.customerOrder = JSON.stringify(dataCart);

        checkCart();
        getPriceTotal();

        if (dataCart[el].count < 1) {
          cart.parentElement.remove();
          delete dataCart[el];
          localStorage.customerOrder = JSON.stringify(dataCart);
          checkCart();
          getPriceTotal();
        }
      }
    });
  });

  spanPlus.addEventListener("click", (e) => {
    dataBase.forEach((el) => {
      if (el.id === obj.id) {
        el.count++;
        sessionStorage.productBase = JSON.stringify(dataBase);
      }
    });
    Object.keys(dataCart).forEach((el) => {
      if (el === obj.id) {
        obj.count++;
        dataCart[el].count++;
        spanCount.textContent = dataCart[el].count;
        cartPrice.textContent = `${(
          dataCart[el].price * dataCart[el].count
        ).toFixed(2)}`;
        localStorage.customerOrder = JSON.stringify(dataCart);
        checkCart();
        getPriceTotal();
      }
    });
  });
  return cart;
}

window.addEventListener("DOMContentLoaded", () => {
  showProductCard(Object.values(dataCart), cartBox, createCart);
  getPriceTotal();
});
