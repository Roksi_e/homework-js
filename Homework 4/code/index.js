//---Задача 1-----------------------

const arr = [9, 33, 11, -2, 0, -99, 44, 115];
const newArr = [];

function map(fun, array) {
  for (let item of array) {
    newArr.push(item);
  }
  fun(newArr);
  return newArr;
}

function fun(array) {
  return array.sort((a, b) => (a > b ? 1 : -1));
}

map(fun, arr);
console.log(arr);
console.log(newArr);

//---Задача 2----------------------------

let userAge = prompt('Скільки тобі років?');

function checkAge(age) {
  return age > 18
    ? true
    : confirm('Батьки дозволили?')
    ? true
    : alert('Ти ще малий');
}

checkAge(userAge);

function checkAgeTwo(age) {
  return age > 18 || confirm('Батьки дозволили?');
}

checkAgeTwo(userAge);

