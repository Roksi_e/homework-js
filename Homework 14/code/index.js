window.addEventListener("DOMContentLoaded", () => {
  // 1. Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода имени и
  // фамилии

  class Person {
    constructor(firstName, lastName) {
      this.firstName = firstName;
      this.lastName = lastName;
    }
    show() {
      return console.log(
        `Ім'я: ${this.firstName}. Прізвище: ${this.lastName}.`
      );
    }
  }

  const [...userInput] = document.querySelectorAll(".div-user > input"),
    users = [],
    user = {
      name: "",
      surname: "",
    };
  let data = [],
    isValid = "";

  function inputValidate() {
    let item = this;

    if (item.type === "text") {
      if (item.id === "name" && /^[А-яЇїЄєІіҐґ-]+$/i.test(item.value)) {
        user.name = item.value.toLowerCase();
        item.isValid = true;
      } else if (
        item.id === "lastname" &&
        /^[А-яЇїЄєІіҐґ-]+$/i.test(item.value)
      ) {
        user.surname = item.value.toLowerCase();
        item.isValid = true;
      } else {
        alert("Введіть коректно ім'я та прізвище!");
        item.isValid = false;
      }
    }
  }

  if (sessionStorage.dataUser) {
    data = JSON.parse(sessionStorage.dataUser);
    console.log(data);
  }

  userInput.forEach((item) => {
    if (item.type === "text") {
      item.addEventListener("change", inputValidate);
    }

    if (item.type === "button") {
      item.addEventListener("click", (ev) => {
        userInput.forEach(function (item) {
          if (item.type === "text") {
            inputValidate.apply(item);
          }
        });
        if (userInput[0].isValid === true && userInput[1].isValid === true) {
          let person = new Person(user.name, user.surname);
          person.show();
          users.push(person);
          sessionStorage.dataUser = JSON.stringify(users);
        }
      });
    }
  });

  // 2. Создай список состоящий из 4 листов. Используя джс обратись к 2 li и с использованием навигации по DOM дай 1 элементу
  // синий фон, а 3 красный

  const ul = document.createElement("ul"),
    divLi = document.querySelector(".div-li"),
    items = ["Item 1", "Item 2", "Item 3", "Item 4"],
    lis = items.map((item) => {
      const li = document.createElement("li");
      li.textContent = item;
      return li;
    });

  ul.append(...lis);
  divLi.append(ul);

  lis[1].nextElementSibling.classList.add("red");
  lis[1].previousElementSibling.classList.add("blue");

  // 3. Создай див высотой 400 пикселей и добавь на него событие наведения мышки. При наведении мышки выведите текстом
  // координаты, где находится курсор мышки

  const divCub = document.createElement("div");
  divCub.classList.add("div-mouse");
  divLi.after(divCub);

  divCub.addEventListener("mousemove", (e) => {
    divCub.innerText = `Подія 'mousemove' X: ${e.offsetX}  Y: ${e.offsetY}`;
  });

  // 4. Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том какая кнопка была нажата

  const divBtn = document.createElement("div"),
    inputBtn = [1, 2, 3, 4],
    buttons = inputBtn.map((item) => {
      const btn = document.createElement("input");
      btn.type = "button";
      btn.value = item;
      return btn;
    });

  divBtn.classList.add("div-btn");
  divCub.after(divBtn);
  divBtn.append(...buttons);

  buttons.forEach((el) => {
    el.addEventListener("click", (e) => {
      alert(`Натиснута кнопка ${e.target.value}`);
    });
  });

  // 5. Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице

  const divAnime = document.querySelector(".anime");
  const divMove = document.createElement("div");
  divMove.classList.add("div-move");
  divAnime.append(divMove);

  let moveDiv;
  let anime;

  function moveRight() {
    moveDiv = 0;
    anime = setInterval(animateRight, 10);
  }
  function animateRight() {
    let width = document.documentElement.clientWidth;
    if (moveDiv == width - parseInt(getComputedStyle(divMove).width) - 20) {
      clearInterval(anime);
    } else {
      moveDiv++;
      divMove.style.left = `${moveDiv}px`;
    }
  }

  divMove.addEventListener("mouseover", (e) => {
    clearInterval(anime);
    moveRight();
    setTimeout(function () {
      divMove.style.left = "5px";
    }, 20000);
  });

  // 6. Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body

  const inputColor = document.createElement("input"),
    btnColor = document.createElement("input"),
    divColor = document.createElement("div");

  inputColor.type = "color";
  btnColor.type = "button";
  btnColor.value = "Змінити колір";
  divColor.classList.add("div-color");
  inputColor.classList.add("input-color");
  btnColor.classList.add("btn-color");
  divAnime.after(divColor);
  divColor.append(inputColor, btnColor);

  let userColor = document.querySelector(".input-color");

  divColor.insertAdjacentText("beforebegin", "Оберіть колір головної сторінки");

  btnColor.addEventListener("click", (e) => {
    document.body.style.backgroundColor = userColor.value;
  });

  // 7. Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль

  const divLogin = document.createElement("div"),
    inputLogin = document.createElement("input");

  inputLogin.type = "text";
  inputLogin.placeholder = "Введіть логін";

  divLogin.classList.add("div-login");
  divColor.after(divLogin);
  divLogin.append(inputLogin);

  inputLogin.addEventListener("input", (e) => {
    console.log(e.target.value);
  });

  // 8. Создайте поле для ввода данных поле введения данных выведите текст под полем

  const infoUser = document.createElement("textarea");

  infoUser.placeholder = "Напишіть щось";
  infoUser.classList.add("info-user");
  divLogin.after(infoUser);

  infoUser.addEventListener("change", (e) => {
    const divInfo = document.createElement("div");
    divInfo.classList.add("div-info");
    e.target.after(divInfo);
    divInfo.insertAdjacentText("beforeend", `${e.target.value}`);
  });
});
