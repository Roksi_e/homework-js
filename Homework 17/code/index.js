import { req, show } from "./function.js";
/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 
Курс валют НБУ з датою на який день,  https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json
героїв зоряних війн, https://swapi.dev/api/people/
список справ з https://jsonplaceholder.typicode.com/ виводити які виконані які та які ні з можливістю редагування
*/
if (localStorage.history === undefined) {
  localStorage.history = JSON.stringify([]);
}

document.querySelector(".navigation").addEventListener("click", (e) => {
  if (e.target.id === "todo") {
    req("https://jsonplaceholder.typicode.com/todos").then((info) =>
      show(info, e.target.id)
    );
  } else if (e.target.id === "star-wars") {
    req("https://swapi.dev/api/people/").then((info) => {
      show(info.results, e.target.id);
    });
  } else if (e.target.id === "nbua") {
    req(
      "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    ).then((info) => show(info, e.target.id));
  }
});

document
  .querySelector("#close")
  .addEventListener("click", (e) =>
    document.querySelector(".parent").classList.remove("active")
  );
