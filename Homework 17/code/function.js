const spans = [];
const btns = [1, 2, 3, 4, 5, 6, 7, 8, 9];

async function req(url) {
  document.querySelector(".loader").classList.add("active");
  const data = await fetch(url);
  return data.json();
}

function showStarWars(data, element) {
  const tbody = document.createElement("tbody");
  data.map((obj) => {
    const tr = document.createElement("tr");
    const tdName = document.createElement("td");
    const tdBirth = document.createElement("td");
    const tdGender = document.createElement("td");
    const tdHeight = document.createElement("td");
    const tdFilms = document.createElement("td");
    const tdHomeworld = document.createElement("td");
    const tdEyeColor = document.createElement("td");

    req(obj.homeworld).then((planet) => {
      tdHomeworld.textContent = planet.name;
    });

    obj.films.forEach((item) => {
      const ul = document.createElement("ul");
      req(item).then((film) => {
        const li = document.createElement("li");
        li.textContent = film.title;
        ul.append(li);
      });
      tdFilms.append(ul);
    });

    tdName.textContent = obj.name;
    tdBirth.textContent = obj.birth_year;
    tdGender.textContent = obj.birth_year;
    tdHeight.textContent = obj.height;
    tdEyeColor.textContent = obj.eye_color;

    tbody.append(tr);
    tr.append(
      tdName,
      tdBirth,
      tdGender,
      tdHeight,
      tdFilms,
      tdHomeworld,
      tdEyeColor
    );
  });

  element.insertAdjacentElement("beforeend", tbody);
  document.querySelector(".loader").classList.remove("active");
}

function clearClass(array) {
  array.forEach((item) => {
    item.classList.remove("active-btn");
  });
}

function clickBtn() {
  const [...footBtn] = document.querySelectorAll("footer > button");
  footBtn.forEach((item) => {
    const table = document.querySelector("table");
    item.addEventListener("click", (e) => {
      clearClass(footBtn);
      e.target.classList.add("active-btn");
      if (e.target.textContent === "1") {
        return;
      } else if (e.target.textContent === "2") {
        req("https://swapi.dev/api/people/?page=2").then((info) => {
          showStarWars(info.results, table);
        });
      } else if (e.target.textContent === "3") {
        req("https://swapi.dev/api/people/?page=3").then((info) => {
          showStarWars(info.results, table);
        });
      } else if (e.target.textContent === "4") {
        req("https://swapi.dev/api/people/?page=4").then((info) => {
          showStarWars(info.results, table);
        });
      } else if (e.target.textContent === "5") {
        req("https://swapi.dev/api/people/?page=5").then((info) => {
          showStarWars(info.results, table);
        });
      } else if (e.target.textContent === "6") {
        req("https://swapi.dev/api/people/?page=6").then((info) => {
          showStarWars(info.results, table);
        });
      } else if (e.target.textContent === "7") {
        req("https://swapi.dev/api/people/?page=7").then((info) => {
          showStarWars(info.results, table);
        });
      } else if (e.target.textContent === "8") {
        req("https://swapi.dev/api/people/?page=8").then((info) => {
          showStarWars(info.results, table);
        });
      } else if (e.target.textContent === "9") {
        req("https://swapi.dev/api/people/?page=9").then((info) => {
          showStarWars(info.results, table);
        });
      }
    });
  });
}

function createBtn() {
  btns.forEach((item) => {
    const btn = document.createElement("button");
    btn.textContent = `${item}`;
    btn.classList.add("footer__btn");
    document.querySelector("footer").append(btn);
  });
}

function show(data, target) {
  const display = document.querySelector(".display");
  document.querySelector("footer").innerHTML = "";
  display.innerHTML = "";
  if (target === "todo") {
    const table = `
        <table>
         <thead>
          <tr>
           <th>
           №
           </th>
           <th>
           Задача
           </th>
           <th>
           Статус
           </th>
           <th>
           Редагувати
           </th>
          </tr>
         </thead>
         <tbody>
           ${data
             .map((obj, i) => {
               const span = document.createElement("span");
               span.addEventListener("click", () => {
                 clickHendler(obj);
               });
               span.innerHTML = "&#128295;";
               spans.push(span);
               return `
               <tr>
                    <td>
                    ${obj.id}
                    </td>
                    <td>
                    ${obj.title}
                    </td>
                    <td>
                    ${obj.completed ? "&#9989;" : "&#10060;"}
                    </td>
                    <td data-span="${i}">
                      
                    </td>
                </tr>`;
             })
             .join("")}
         </tbody>
         </tablr>
        `;
    display.insertAdjacentHTML("beforeend", table);
    document.querySelectorAll("td[data-span]").forEach((e, i) => {
      console.log();
      e.append(spans[i]);
    });
  } else if (target === "nbua") {
    const table = `
    <table>
    <thead>    
      <tr>
        <th>№</th>
        <th>Назва валюти</th>
        <th>Ціна валюти</th>
      </tr>
    </thead>
    <tbody>
    ${data
      .map((obj, i) => {
        return `
        <tr>
        <td>${i + 1}</td>
        <td>${obj.txt}</td>
        <td>${obj.rate}</td>
      </tr>
      `;
      })
      .join("")}    
    </tbody>
  </table>
    `;
    display.insertAdjacentHTML("beforeend", table);
  } else if (target === "star-wars") {
    createBtn();
    const table = document.createElement("table");
    const thead = `   
    <thead>
      <tr>
        <th>Ім'я героя</th>
        <th>Дата народження</th>
        <th>Стать</th>
        <th>Зріст</th>
        <th>Фільми</th>
        <th>Планета</th>
        <th>Колір очей</th>
      </tr>
    </thead>   
    `;
    display.append(table);
    table.insertAdjacentHTML("beforeend", thead);
    showStarWars(data, table);
    clickBtn();
  }

  document.querySelector(".loader").classList.remove("active");
}

function clickHendler(obj) {
  document.querySelector(".parent").classList.add("active");
  document.getElementById("description").innerText = obj.title;
  document.getElementById("status").checked = obj.completed;

  document.getElementById("save").onclick = () => {
    console.log("+");
    const l = JSON.parse(localStorage.history);
    l.push(obj);
    localStorage.history = JSON.stringify(l);

    document
      .querySelector("#save")
      .addEventListener("click", (e) =>
        document.querySelector(".parent").classList.remove("active")
      );
  };
}

export { req, show };
