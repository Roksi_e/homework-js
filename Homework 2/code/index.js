//---Задача 1 -------------------------------------

let k = 10;

for (let i = 0; i < 10; i++) {
  for (let j = 0; j < 10; j++) {
    if (j < k) {
      document.write('&nbsp');
    } else {
      document.write('*');
    }
  }
  k--;
  document.write('<br>');
}
document.write('<br>');
document.write('<hr>');

//------------------------------------------------

let c = 10;

for (let i = 0; i < 10; i++) {
  for (let j = 0; j < 10; j++) {
    if (j < c) {
      document.write('&nbsp');
    } else {
      document.write('*');
    }
  }
  c--;
  document.write('<br>');
}
for (let i = 0; i < 10; i++) {
  for (let j = 0; j < 10; j++) {
    if (j < c) {
      document.write('&nbsp');
    } else {
      document.write('*');
    }
  }
  c++;
  document.write('<br>');
}
document.write('<br>');
document.write('<hr>');

//-----------------------------------------------

for (let i = 0; i < 19; i++) {
  document.write('* ');
}
document.write('<br>');
for (let i = 0; i < 8; i++) {
  document.write('*');
  for (j = 0; j < 52; j++) {
    document.write('&nbsp');
  }
  document.write('*');
  document.write('<br>');
}

for (let i = 0; i < 19; i++) {
  document.write('* ');
}
document.write('<br>');
document.write('<hr>');

//---Задача 2-----------------------------------

let number = parseFloat(prompt('Введіть ціле число'));

for (let i = 1; i <= number; i++) {
  if ((number ^ 0) !== number) {
    number = parseFloat(prompt('Ви ввели не те число. Введіть ціле число'));
  } else {
    if (i % 5 === 0) {
      console.log(i);
    } else if (number < 5) {
      console.error('Sorry, no numbers');
    }
  }
}
alert(`Ви ввели число ${number}`);

//-----------------------------------------------------------

let m = parseFloat(prompt('Введіть меньше число'));
let n = parseFloat(prompt('Введіть більше число'));

while ((m ^ 0) !== m || (n ^ 0) !== n || m > n) {
  alert('Ви ввели невірне число. Введіть ще раз');
  m = parseFloat(prompt('Введіть меньше число'));
  n = parseFloat(prompt('Введіть більше число'));
}

nextPrime: for (let i = m; i <= n; i++) {
  for (let j = 2; j < i; j++) {
    if (i % j == 0) continue nextPrime;
  }
  console.log(i);
}
